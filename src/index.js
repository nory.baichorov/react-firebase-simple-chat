import React, { createContext } from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';

firebase.initializeApp({
    apiKey: "AIzaSyC_limwt2WTN9V4bsDfJ2JyNcNavb6YxiY",
    authDomain: "react-chat-aa8f8.firebaseapp.com",
    projectId: "react-chat-aa8f8",
    storageBucket: "react-chat-aa8f8.appspot.com",
    messagingSenderId: "1050350014449",
    appId: "1:1050350014449:web:033fc71c4a402484dcfe50",
    measurementId: "G-3238J1FBYQ"
});

export const Context = createContext(null);
const auth = firebase.auth();
const firestore = firebase.firestore();

ReactDOM.render(
  <React.StrictMode>
      <Context.Provider value={{ firebase, auth, firestore }}>
          <App />
      </Context.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
