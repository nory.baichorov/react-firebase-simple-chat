import React, { useContext, useState } from 'react';
import { Context } from "../index";
import { useAuthState } from "react-firebase-hooks/auth";
import { Avatar, Button, Container, Grid, TextField } from "@mui/material";
import { useCollectionData } from "react-firebase-hooks/firestore";
import Loader from "./Loader";
import firebase from 'firebase';
import '../App.css';

const Chat = () => {
    const { auth, firestore } = useContext(Context);
    const [user] = useAuthState(auth);
    const [value, setValue] = useState('');

    const [messages, loading] = useCollectionData(
        firestore.collection('messages').orderBy('createdAt')
    );

    const sendMessage = async () => {
        firestore.collection('messages').add({
            uid: user.uid,
            displayName: user.displayName,
            photoURL: user.photoURL,
            text: value,
            createdAt: firebase.firestore.FieldValue.serverTimestamp()
        });
        setValue('');
    }

    if (loading) {
         return <Loader />
    }

    return (
        <Container>
            <Grid
                container
                justifyContent={'center'}
                style={{height: window.innerHeight - 50, marginTop: 20}}>
                <div className='chatWrapper'>
                    {messages.map(message =>
                        <div style={{
                            margin: 10,
                            border: user.uid === message.uid ? '1px solid black' : '2px solid white',
                            borderRadius: 5,
                            marginLeft: user.uid === message.uid ? 'auto' : '10px',
                            minWidth: 200,
                            minHeight: 80,
                            height: 'fit-content',
                            width: 'fit-content',
                            padding: 5,
                        }}>
                            <Grid container justifyContent={'space-between'}>
                                <Avatar src={message.photoURL}/>
                                <div>{message.displayName}</div>
                            </Grid>
                            <div style={{margin: '10px 0 0 6px'}}>
                                {message.text}
                            </div>
                        </div>
                    )}
                </div>
                <Grid
                    container
                    direction={'column'}
                    alignItems={'flex-end'}
                    style={{width: '80%'}}>
                    <TextField
                        fullWidth
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        maxRows={2}
                        variant={'outlined'}/>
                    <Button
                        onClick={sendMessage}
                        color={'primary'}
                        variant={'outlined'}
                        style={{marginTop: 5}}>Отправить</Button>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Chat;