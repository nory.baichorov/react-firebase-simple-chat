import React, {useContext} from 'react';
import { Box, Button, Container, Grid } from "@mui/material";
import { Context } from "../index";
import firebase from 'firebase';
import '../App.css';
import google from '../assets/google.svg';

const Login = () => {
    const { auth } = useContext(Context);

    const login = async () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        const { user } = await auth.signInWithPopup(provider);
        console.log(user);
    }
    return (
        <Container>
            <Grid container style={{
                height: window.innerHeight - 50,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Grid style={{width: 400}}
                        container
                        alignItems={'center'}
                        direction={'column'}>
                    <Box p={4}>
                        <Button
                            className={'btnGoogle'}
                            onClick={login}>
                            <img src={google} alt={'google-logo'}/>
                            Войти с помощью Google
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Login;