import React, { useContext } from 'react';
import { AppBar, Button, Grid, Toolbar } from "@mui/material";
import { NavLink } from "react-router-dom";
import { LOGIN_ROUTE } from "../utils/consts";
import { Context } from "../index";
import '../App.css';
import { useAuthState } from "react-firebase-hooks/auth";

const NavBar = () => {
    const { auth } = useContext(Context);
    const [user] = useAuthState(auth);

    return (
        <AppBar className='appbar' position="static">
            <Toolbar variant={'dense'}>
                <Grid container justifyContent={'flex-end'}>
                    {user
                        ? <Grid>
                            <span className={'welcome'}>
                                Добро пожаловать, {user.displayName}!
                            </span>
                            <Button
                                style={{marginLeft: 10}}
                                color={'primary'}
                                variant={'outlined'}
                                onClick={() => auth.signOut()}>Выйти</Button>
                          </Grid>
                        : undefined
                    }
                </Grid>
            </Toolbar>
        </AppBar>
    );
};

export default NavBar;