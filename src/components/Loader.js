import React from 'react';
import { Box, Container, Grid } from "@mui/material";

const Loader = () => {
    return (
        <Container>
            <Grid container style={{
                height: window.innerHeight - 50,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Grid style={{width: 400}}
                      container
                      alignItems={'center'}
                      direction={'column'}>
                    <Box p={4}>
                        <div className="lds-roller">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Loader;